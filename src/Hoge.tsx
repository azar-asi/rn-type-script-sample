import React, {PureComponent} from 'react';
import { View, Text } from 'react-native';

export default class Hoge extends PureComponent {
    render() {
        return (
            <View>
                <Text>Hoge class test</Text>
            </View>
        );
    }
}